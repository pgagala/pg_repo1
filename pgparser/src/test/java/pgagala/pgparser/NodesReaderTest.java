package pgagala.pgparser;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;



@RunWith(JUnit4.class)
public class NodesReaderTest {
	
	private NodesReader nodesReader = new NodesReader();
	
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();
	
	private Sheet basicSheet(Workbook wb){
		Sheet sheet = wb.createSheet("new sheet");
	    Row row = sheet.createRow((short)0);
	    row.createCell(0).setCellValue("Poziom 1");
	    row.createCell(1).setCellValue("Poziom 2");
	    row.createCell(2).setCellValue("ID");
	    
	    return sheet;
	}
	
	@Test
	public void firstColumnMissing() throws Exception{
		Workbook wb = new HSSFWorkbook();
	    Sheet sheet = wb.createSheet("new sheet");

	    expectedEx.expect(NodeParseException.class);
	    expectedEx.expectMessage(ErrorMessage.MISSING_HEADER_ROW.getMessage());
	    
	    List<Node> nodes = nodesReader.readNodes(wb);
	}
	
	@Test
	public void emptyNodeList() throws Exception{
		Workbook wb = new HSSFWorkbook();
	    Sheet sheet = basicSheet(wb);

	    List<Node> nodes = nodesReader.readNodes(wb);
	    Assert.assertTrue(nodes.isEmpty());
	}
	
	@Test
	public void nonNumericID() throws Exception{
		Workbook wb = new HSSFWorkbook();
	    Sheet sheet = basicSheet(wb);

	    Row row = sheet.createRow((short)1);
	    row.createCell(0).setCellValue("A");
	    row.createCell(1).setCellType(CellType.BLANK);
	    row.createCell(2).setCellValue("This is a string");
	    
	    expectedEx.expect(NodeParseException.class);
	    expectedEx.expectMessage(ErrorMessage.NOT_NUMERIC_ID.getMessage());
	    
	    nodesReader.readNodes(wb);
	}
	
	@Test
	public void shouldBeIntegerID() throws Exception{
		Workbook wb = new HSSFWorkbook();
	    Sheet sheet = basicSheet(wb);

	    Row row = sheet.createRow((short)1);
	    row.createCell(0).setCellValue("A");
	    row.createCell(1).setCellType(CellType.BLANK);
	    row.createCell(2).setCellValue(1.4);
	    
	    expectedEx.expect(NodeParseException.class);
	    expectedEx.expectMessage(ErrorMessage.ID_SHOULD_BE_INTEGER.getMessage());
	    
	    nodesReader.readNodes(wb);
	}
	
	@Test
	public void readsNodesSuccessfully() throws Exception {
		Workbook wb = new HSSFWorkbook();
		Sheet sheet = wb.createSheet("new sheet");
	    Row row = sheet.createRow((short)0);
	    row.createCell(0).setCellValue("Poziom 1");
	    row.createCell(1).setCellValue("Poziom 2");
	    row.createCell(2).setCellValue("Poziom 3");
	    row.createCell(3).setCellValue("ID");
	    
	    row = sheet.createRow((short)1);
	    row.createCell(0).setCellValue("A");
	    row.createCell(1).setCellType(CellType.BLANK);
	    row.createCell(2).setCellType(CellType.BLANK);
	    row.createCell(3).setCellValue(1);
	    
	    row = sheet.createRow((short)2);
	    row.createCell(0).setCellType(CellType.BLANK);
	    row.createCell(1).setCellValue("B");
	    row.createCell(2).setCellType(CellType.BLANK);
	    row.createCell(3).setCellValue(2);
	    
	    row = sheet.createRow((short)3);
	    row.createCell(0).setCellType(CellType.BLANK);
	    row.createCell(1).setCellType(CellType.BLANK);
	    row.createCell(2).setCellValue("B1");
	    row.createCell(3).setCellValue(3);
	    
	    row = sheet.createRow((short)4);
	    row.createCell(0).setCellType(CellType.BLANK);
	    row.createCell(1).setCellType(CellType.BLANK);
	    row.createCell(2).setCellValue("B2");
	    row.createCell(3).setCellValue(4);
	    
	    List<Node> nodes = nodesReader.readNodes(wb);
	    List<Node> expectedNodes = new ArrayList<Node>();
	    Node a = new Node(1, "A");
	    Node b = new Node(2, "B");
	    Node b1 = new Node(3, "B1");
	    Node b2 = new Node(4, "B2");
	    expectedNodes.add(a);
	    a.addNode(b);
	    b.addNode(b1);
	    b.addNode(b2);
	    
	    Assert.assertEquals(expectedNodes, nodes);
	}
	
	@Test
	public void missingNodeName() throws Exception {
		Workbook wb = new HSSFWorkbook();
	    Sheet sheet = basicSheet(wb);

	    Row row = sheet.createRow((short)1);
	    row.createCell(0).setCellType(CellType.BLANK);
	    row.createCell(1).setCellType(CellType.BLANK);
	    row.createCell(2).setCellValue(1);
	    
	    expectedEx.expect(NodeParseException.class);
	    expectedEx.expectMessage(ErrorMessage.MISSING_NODE_NAME.getMessage());
	    
	    nodesReader.readNodes(wb);
	}
	
	@Test
	public void incorrectNodeLevel_firstRow() throws Exception {
		Workbook wb = new HSSFWorkbook();
	    Sheet sheet = wb.createSheet("new sheet");
	    Row row = sheet.createRow((short)0);
	    row.createCell(0).setCellValue("Poziom 1");
	    row.createCell(1).setCellValue("Poziom 2");
	    row.createCell(1).setCellValue("Poziom 3");
	    row.createCell(2).setCellValue("ID");
	    
	    row = sheet.createRow((short)1);
	    row.createCell(0).setCellType(CellType.BLANK);
	    row.createCell(1).setCellValue("A");
	    row.createCell(2).setCellType(CellType.BLANK);
	    row.createCell(3).setCellValue(1);
	    
	    expectedEx.expect(NodeParseException.class);
	    expectedEx.expectMessage(ErrorMessage.INCORRECT_LEVEL.getMessage());
	    
	    nodesReader.readNodes(wb);
	}
	
	@Test
	public void incorrectNodeLevel_secondRow() throws Exception{
		Workbook wb = new HSSFWorkbook();
	    Sheet sheet = wb.createSheet("new sheet");
	    Row row = sheet.createRow((short)0);
	    row.createCell(0).setCellValue("Poziom 1");
	    row.createCell(1).setCellValue("Poziom 2");
	    row.createCell(1).setCellValue("Poziom 3");
	    row.createCell(2).setCellValue("ID");
	    
	    row = sheet.createRow((short)1);
	    row.createCell(1).setCellValue("A");
	    row.createCell(0).setCellType(CellType.BLANK);
	    row.createCell(2).setCellType(CellType.BLANK);
	    row.createCell(3).setCellValue(1);
	    
	    row = sheet.createRow((short)2);
	    row.createCell(0).setCellType(CellType.BLANK);
	    row.createCell(2).setCellType(CellType.BLANK);
	    row.createCell(1).setCellValue("B");
	    row.createCell(3).setCellValue(2);
	    
	    expectedEx.expect(NodeParseException.class);
	    expectedEx.expectMessage(ErrorMessage.INCORRECT_LEVEL.getMessage());
	    
	    nodesReader.readNodes(wb);
	}
	
	@Test
	public void multipleNamesForNode() throws Exception{
		Workbook wb = new HSSFWorkbook();
	    Sheet sheet = basicSheet(wb);

	    Row row = sheet.createRow((short)1);
	    row.createCell(0).setCellValue("A");
	    row.createCell(1).setCellValue("B");
	    row.createCell(2).setCellValue(1);
	    
	    expectedEx.expect(NodeParseException.class);
	    expectedEx.expectMessage(ErrorMessage.MULTIPLE_NAME.getMessage());
	    
	    nodesReader.readNodes(wb);
	}
	
	@Test
	public void nameShouldBeNonEmptyString() throws Exception{
		Workbook wb = new HSSFWorkbook();
	    Sheet sheet = basicSheet(wb);

	    Row row = sheet.createRow((short)1);
	    row.createCell(0).setCellValue("");
	    row.createCell(1).setCellValue("");
	    row.createCell(2).setCellValue(1);
	    
	    expectedEx.expect(NodeParseException.class);
	    expectedEx.expectMessage(ErrorMessage.EMPTY_NODE_NAME.getMessage());
	    
	    nodesReader.readNodes(wb);
	}
	
	@Test
	public void missingIDColumn() throws Exception{
		Workbook wb = new HSSFWorkbook();
	    Sheet sheet = wb.createSheet("new sheet");
	    Row row = sheet.createRow((short)0);
	    row.createCell(0).setCellValue("Poziom 1");
	    row.createCell(1).setCellValue("Poziom 2");
	    row.createCell(1).setCellValue("Poziom 3");
	    
	    row = sheet.createRow((short)1);
	    row.createCell(1).setCellValue("A");
	    row.createCell(0).setCellType(CellType.BLANK);
	    row.createCell(2).setCellType(CellType.BLANK);
	    row.createCell(3).setCellValue(1);
	    
	    expectedEx.expect(NodeParseException.class);
	    expectedEx.expectMessage(ErrorMessage.MISSING_ID_COLUMN.getMessage());
	    
	    nodesReader.readNodes(wb);
	}
	
	@Test
	public void idShouldBeUnique() throws Exception{
		Workbook wb = new HSSFWorkbook();
	    Sheet sheet = wb.createSheet("new sheet");
	    Row row = sheet.createRow((short)0);
	    row.createCell(0).setCellValue("Poziom 1");
	    row.createCell(1).setCellValue("Poziom 2");
	    row.createCell(2).setCellValue("Poziom 3");
	    row.createCell(3).setCellValue("ID");
	    
	    row = sheet.createRow((short)1);
	    row.createCell(0).setCellValue("A");
	    row.createCell(1).setCellType(CellType.BLANK);
	    row.createCell(2).setCellType(CellType.BLANK);
	    row.createCell(3).setCellValue(1);
	    
	    row = sheet.createRow((short)2);
	    row.createCell(0).setCellValue("B");
	    row.createCell(1).setCellType(CellType.BLANK);
	    row.createCell(2).setCellType(CellType.BLANK);
	    row.createCell(3).setCellValue(1);
	    
	    expectedEx.expect(NodeParseException.class);
	    expectedEx.expectMessage(ErrorMessage.ID_SHOULD_BE_UNIQUE.getMessage());
	    
	    nodesReader.readNodes(wb);
	}
	
}
