package pgagala.pgparser;

public class NodeParseException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NodeParseException(ErrorMessage errorMessage){
		super(errorMessage.getMessage());
	}
}
