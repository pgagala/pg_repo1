package pgagala.pgparser;

import java.util.ArrayList;
import java.util.List;

public class Node {
	private int id;
	private String name;
	private List<Node> nodes;
	
	public Node(int id, String name) {
		super();
		if(name==null || name.length()==0)throw new RuntimeException("Name should be non-empty");
		this.id = id;
		this.name = name;
		nodes = new ArrayList<>();
	}
	
	public void addNode(Node n){
		nodes.add(n);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public List<Node> getNodes() {
		return new ArrayList<>(nodes);
	}
	
	@Override
	public String toString(){
		return String.format("Node(%s,%d,%s)", name, id, nodes);
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof Node){
			Node n = (Node)o;
			return n.id==this.id && n.name.equals(this.name) && n.nodes.equals(this.nodes);
		}
		return false;
	}
	
	@Override
	public int hashCode(){
		return 37 * id * name.hashCode() * nodes.hashCode();
	}
}
