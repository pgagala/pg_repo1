package pgagala.pgparser;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class NodesReader {
	
	List<Node> readNodes(Workbook workbook) throws NodeParseException {
		List<Node> nodes = new ArrayList<>();
		Sheet datatypeSheet = workbook.getSheetAt(0);
		Iterator<Row> rowsIt = datatypeSheet.iterator();
		if(!rowsIt.hasNext())throw new NodeParseException(ErrorMessage.MISSING_HEADER_ROW);
		Row currentRow = rowsIt.next();
		int columnCount = getColumnCount(currentRow);
		int level = 0;
		Node[] currentProcessedNodes = new Node[columnCount];
		Set<Integer> nodeIds = new HashSet<>();
		while (rowsIt.hasNext()) {

			currentRow = rowsIt.next();
			

			
			NodeFromRow nfr = readNode(currentRow,level,columnCount);
			Node node = nfr.node;
			if(nodeIds.contains(node.getId())){
				throw new NodeParseException(ErrorMessage.ID_SHOULD_BE_UNIQUE);
			}else{
				nodeIds.add(node.getId());
			}
			level = nfr.level;
			if(level==1){
				nodes.add(node);
			}else{
				Node parent = currentProcessedNodes[level-2];
				parent.addNode(node);
			}
			currentProcessedNodes[level-1]=node;
		}
		
		return nodes;
	}

	private NodeFromRow readNode(Row currentRow,int currentLevel, int columnCount) throws NodeParseException {
		int nodeId=0;
		int thisNodeLevel = 0;
		String name=null;
		
		int cellNum = 1;
		Iterator<Cell> cellIterator = currentRow.iterator();
		while (cellIterator.hasNext()) {

			Cell currentCell = cellIterator.next();
			if(cellNum==columnCount){
				if(currentCell.getCellTypeEnum()==CellType.NUMERIC){
					nodeId = parseID(currentCell);
					break;
				}else{
					throw new NodeParseException(ErrorMessage.NOT_NUMERIC_ID);
				}
			}else{
				if (currentCell.getCellTypeEnum() == CellType.STRING) {
					if(name!=null)throw new NodeParseException(ErrorMessage.MULTIPLE_NAME);
					name = currentCell.getStringCellValue();
					if(name.length()==0)throw new NodeParseException(ErrorMessage.EMPTY_NODE_NAME);
					if(cellNum>currentLevel+1)throw new NodeParseException(ErrorMessage.INCORRECT_LEVEL);
					thisNodeLevel = cellNum;

				} else if (currentCell.getCellTypeEnum() == CellType.BLANK) {
					//
				}
				cellNum++;
			}
			
		}
		if(name==null){
			throw new NodeParseException(ErrorMessage.MISSING_NODE_NAME);
		}
		NodeFromRow nfr = new NodeFromRow();
		nfr.node = new Node(nodeId,name);
		nfr.level = thisNodeLevel;
		return nfr;
	}

	/**
	 * @param currentCell
	 * @return
	 * @throws NodeParseException
	 */
	private int parseID(Cell currentCell) throws NodeParseException {
		double val = currentCell.getNumericCellValue();
		if(Math.floor(val)!=val)throw new NodeParseException(ErrorMessage.ID_SHOULD_BE_INTEGER);
		return ((Double)val).intValue();
	}

	private int getColumnCount(Row firstRow) throws NodeParseException {
		Iterator<Cell> cellIterator = firstRow.iterator();
		int count = 0;
		boolean idColumnPresent = false;
		while (cellIterator.hasNext()) {
			count++;
			Cell currentCell = cellIterator.next();
			if("ID".equals(currentCell.getStringCellValue())){
				idColumnPresent = true;
				break;
			}
		}
		if(!idColumnPresent)throw new NodeParseException(ErrorMessage.MISSING_ID_COLUMN);
		return count;
	}
}
