package pgagala.pgparser;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Reads data from xlsx and outputs as json. 
 *
 */
public class App 
{
	private static Logger log = Logger.getAnonymousLogger();
	private static final String DEFAULT_FILE_NAME = "test1.xlsx";
	
    public static void main( String[] args ) throws Exception
    {
    	String filename = args.length==1?args[0]:DEFAULT_FILE_NAME;
        try(FileInputStream excelFile = new FileInputStream(new File(filename));
        		Workbook workbook = new XSSFWorkbook(excelFile)) {
            List<Node> nodes = new NodesReader().readNodes(workbook);
            System.out.println(new ObjectMapper().writeValueAsString(nodes));
            
        } catch (NodeParseException e) {
        	log.info("NodeParseError: "+e.getMessage());
        } catch (Exception e) {
        	log.info("Error: "+e.getMessage());
        }
    }
}
