package pgagala.pgparser;

public enum ErrorMessage {
	MISSING_NODE_NAME("Missing node name"),
	MULTIPLE_NAME("Multiple names for node"),
	NOT_NUMERIC_ID("Not numeric value for ID"),
	ID_SHOULD_BE_INTEGER("ID should be integer"),
	INCORRECT_LEVEL("Node has incorrect level"),
	MISSING_HEADER_ROW("Missing header row"),
	EMPTY_NODE_NAME("empty node name"),
	MISSING_ID_COLUMN("missing ID column"),
	ID_SHOULD_BE_UNIQUE("ID should be unique");
	
	private String message;
	
	private ErrorMessage(String message){
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
	
	
}
